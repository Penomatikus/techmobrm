![Icon](https://cdn1.iconfinder.com/data/icons/Futurosoft%20Icons%200.5.2/96x96/mimetypes/template_source.png)  
( Replace image url with late app icon )  
  
# App-Name  
Short discription for what the app is useful  
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla gravida ligula euismod risus accumsan, et pellentesque mauris tincidunt. In a enim id eros ornare vehicula at id ipsum. Praesent a maximus massa. Sed gravida ex nibh, vulputate dapibus risus pulvinar ac. Quisque ultricies mollis nunc, a varius velit suscipit nec. Nullam pulvinar pellentesque odio venenatis facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean interdum porta ornare. Fusce vulputate est eget arcu porttitor, at tristique tortor auctor. Fusce vitae purus lorem. Nunc at feugiat sem. In velit quam, rhoncus ultrices lacinia ac, ultrices sit amet sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec mattis vitae nulla a viverra. Nam pretium laoreet elit.  


## System requirements  
* ** Reqirement one **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Reqirement two **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Reqirement three **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Reqirement four **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Reqirement five **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  

## Authorization  
| Authorization | Description |  
| --- | --- |   
| `android.permission.WRITE_EXTERNAL_STORAGE` | Lorem ipsum |  

## Get it
| Version | Location |  
| --- | --- |   
| Preview | [sourceforge](https://sourceforge.net/) |  
| Stable | [Play store](https://play.google.com/store?hl=en)  


## Short preview: App
![Icon](http://www.cs.dartmouth.edu/~campbell/cs65/lecture01/images/sphone2.png)  
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla gravida ligula euismod risus accumsan, et pellentesque mauris tincidunt. In a enim id eros ornare vehicula at id ipsum. Praesent a maximus massa. Sed gravida ex nibh, vulputate dapibus risus pulvinar ac. Quisque ultricies mollis nunc, a varius velit suscipit nec. Nullam pulvinar pellentesque odio venenatis facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean interdum porta ornare. Fusce vulputate est eget arcu porttitor, at tristique tortor auctor. Fusce vitae purus lorem. Nunc at feugiat sem. In velit quam, rhoncus ultrices lacinia ac, ultrices sit amet sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec mattis vitae nulla a viverra. Nam pretium laoreet elit.

## Geek stuff  
This area is for all people who are interested in technical aspects.  
### Third party libraries
* ** Library one **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Library two **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Library three **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Library four **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
* ** Library five **: Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
  
### ( Any other interesting )  
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla gravida ligula euismod risus accumsan, et pellentesque mauris tincidunt. In a enim id eros ornare vehicula at id ipsum. Praesent a maximus massa. Sed gravida ex nibh, vulputate dapibus risus pulvinar ac. Quisque ultricies mollis nunc, a varius velit suscipit nec. Nullam pulvinar pellentesque odio venenatis facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean interdum porta ornare. Fusce vulputate est eget arcu porttitor, at tristique tortor auctor. Fusce vitae purus lorem. Nunc at feugiat sem. In velit quam, rhoncus ultrices lacinia ac, ultrices sit amet sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec mattis vitae nulla a viverra. Nam pretium laoreet elit.



