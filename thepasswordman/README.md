![Icon](https://i.imgur.com/m0BmmGx.png)  

  
# ThePasswordMan 
Diese App soll es den User ermöglichen sein Passwort mit den meistbenutzten Passwörtern im Internet abzugleichen. Hierbei soll ein Graph anzeigt
werden der das Vorkommen von einzelnen Buchstaben bzw. Teilen des Passworts in der Datenbank anzeigt. Es wird einen Downloadbereich geben, sodass neue Passwörter auf das Gerät geladen werden können. Außerdem ist es vorgesehen ein neues Passwort zu erzeugen, mit den vom User ausgewählten Einstellungen (Länge, Zeichen usw.). 


## Systemvoraussetzungen  
* ![Android](https://cdn2.iconfinder.com/data/icons/social-icons-color/512/android-20.png)** Android **: Es wird ein Android-System benötigt
  
## Berechtigungen
Folgende Berechtigungen werden benötigt, damit die App einwandfrei laufen wird. Klicken Sie auf "Quelle" um auf die von Google bereitgestellte Android-Entwicklerseite zu gelangen um weitere Informationen zur Implementation zu erhalten.  

| Berechtigung | Beschreibung |  
| --- | --- |   
| `android.permission.WRITE_EXTERNAL_STORAGE` | "Allows an application to write to external storage." [(Quelle)](https://developer.android.com/reference/android/Manifest.permission.html#WRITE_EXTERNAL_STORAGE) |  
| `android.permission.INTERNET`| "Allows applications to open network sockets." [(Quelle)](https://developer.android.com/reference/android/Manifest.permission.html#INTERNET) |   
| `android.permission.ACCESS_NETWORK_STATE` | "Allows applications to access information about networks." [(Quelle)](https://developer.android.com/reference/android/Manifest.permission.html#ACCESS_NETWORK_STATE)|  
| `android.permission.READ_PHONE_STATE` | "Allows read only access to phone state, including the phone number of the device, current cellular network information, the status of any ongoing calls, and a list of any PhoneAccounts registered on the device." [(Quelle)](https://developer.android.com/reference/android/Manifest.permission.html#READ_PHONE_STATE) |  


## Get it
Es steht zur Zeit nur eine Quelle zur Verfügung.  

| Version | Location |  
| --- | --- |   
| Developer | [Bitbucket](https://bitbucket.org/aass16team10/viktor-schneider.com) |  
| | |  

## Vorschau der App  

![Icon](https://bytebucket.org/Penomatikus/techmobrm/raw/cf189c78b100b8a645a48bbdfc07063cbea87921/thepasswordman/pre2.bmp)  
  
* Kontrolliere wie sicher dein Passwort ist!  
* Schaue nach, welche Buchstaben bzw. Silben man lieber nicht verwenden sollte  
* Lade dir immer die aktuellsten meist benutzen Passwörter herunter  

![Icon](https://bytebucket.org/Penomatikus/techmobrm/raw/cf189c78b100b8a645a48bbdfc07063cbea87921/thepasswordman/pre1.bmp)  

* Erstelle ein sicheres Passwort.  
* Was muss ich beachten beim erstellen eines sicheren Passwortes?  


## Geek stuff  
Dieser Bereich ist für all die jenigen, die sich für mehr interessieren, als der Standard.  

### Third party libraries
* ** - **: Es wurden keine Dritt-Anbieter-Bibliotheken verwendet.  
  
### Open Data
Diese App verwendet eine Open-Data Quelle für die Bereitstellung der angezeigten Daten.  
Was ist "Open-Data":  
`offene Daten sind Daten, die von jedermann frei benutzt, weiterverwendet und geteilt werden können - die einzige Einschränkung betrifft die Verpflichtung zur Nennung des Urherbers` - [opendatahandbook.org](http://opendatahandbook.org/guide/de/what-is-open-data/).  
![Icon](https://cdn2.iconfinder.com/data/icons/bitsies/128/Info-16.png) Die vollständige Erklärung finden Sie auf [opendefinition.org](http://opendefinition.org/od/2.0/en/)  




